﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing.Printing;

namespace WebApplication3.Controllers
{
    public class ITController : Controller
    {
        [HttpGet]
        // GET: Tests
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ITTools()
        {
            return View();
        }
        public ActionResult FormMethod(string name, int nombre)
        {

            string Message = string.Format($"Hello, {name} and {nombre}");
            ViewBag.Go = string.Format($"Hello, {name} and {nombre}");

            return View("ITTools");
        } 
    }
}